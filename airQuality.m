%MATLAB R2020b
%name:airQuality
%author:kacpdeb877
%date: 16.11.2020
%version: v1.0

clear; clc;

%Początkowe zdefiniowanie liczby godzin i czasu które później będą dodawane
hours = 0;
days = 0;

%utworzenie pliku tekstowego w którym zapisywane będą obliczenia
result = fopen('PMresoult.txt', 'w');

%stworzenie macierzy zawierającej dopsuczalne normy dla wartości podanych w
%tabelach z danymi
normy = [200,0,0,0,120,0,10000,0,350];


data1 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-02.csv");%otwarcie pliku z danymi
data1(25:27,:) = [];%odrzucenie z tabeli danych wartości niepotrzebnych (trzech ostatnich wierszy)
data1(:,1) = [];%(godziny)

%pętla utworzona aby zastąpić zmienne w pierwszech kolkumnie zmiennymi typu
%double
for i=1:24
    a = data1(i,1);%przyporządkowanie zmiennej a wartości kolejnych komórek
    a = table2array(a);
    a = replace(a,',','.');%zamiana ',' na '.' w dancej komórce
    %zmiany ttypu zmiennej
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;%dodanie do nowej macierzy wartości zmienionej komórki
    
end
data_kolumna = table(data_kolumna);%zamiana macierzy na tabele
data1(:,1) = [];%usniącie kolumny zawierającej niezmienione komórki
data1(:,9) = data_kolumna;%utworzenie nowej kolumny zawierającej poprawne zmienne
data1 = table2array(data1);%zmiana tabeli na macierz
data_kolumna = [];%wyzerowanie utworzonej tabeli aby mogła zostać wykorzystana ponownie

roznica = data1 - normy;%utworzenie macierzy w której wartości nie przekraczające norm godzinowych i ośmiogodzinowy będą ujemne
[n,m]=size(roznica);%wyznaczenie ilości wierszy jako n

%utworzenie kolejnych jednowymiarowych macieży zawierających badane dane
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);

%obliczanie średnich wartości danych dla danej doby
so2_24 =(sum(data1(:,9)))/n;
pm10_24 =(sum(data1(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");%informacja wyświerlająca się gdy wartość dopuszczalna zostanie przekroczona
        fprintf(result, "\nDnia: 02-11-2020");%wypisanie w jakim dniu została ona przekroczona
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));%obliczenie i wypisanie o jaką wartość została ona przekroczona
        days = days + 1;%dodanie dnia lub godziny w której została ona przekroczona do sumy
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 02-11-2020", day1);
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

%następujące wartości zostały obliczone w pętli for ponieważ należało
%sprawdzić każdą komórke w danej macierzy
for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 02-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 02-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 02-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 02-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end

%czynności te zostały powtórzony dla pozstałych dni
data2 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-03.csv");
data2(25:27,:) = [];
data2(:,1) = [];
for i=1:24
    a = data2(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data2(:,1) = [];
data2(:,9) = data_kolumna;
data2 = table2array(data2);
data_kolumna = [];

roznica = data2 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data2(:,9)))/n;
pm10_24 =(sum(data2(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 03-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 03-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 03-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 03-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 03-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 03-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data3 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-04.csv");
data3(25:27,:) = [];
data3(:,1) = [];
for i=1:24
    a = data3(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data3(:,1) = [];
data3(:,9) = data_kolumna;
data3 = table2array(data3);
data_kolumna = [];
roznica = data3 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data3(:,9)))/n;
pm10_24 =(sum(data3(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 04-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 04-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 04-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 04-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 04-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 04-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data4 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-05.csv");
data4(25:27,:) = [];
data4(:,1) = [];
for i=1:24
    a = data4(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data4(:,1) = [];
data4(:,9) = data_kolumna;
data4 = table2array(data4);
data_kolumna = [];

roznica = data4 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data4(:,9)))/n;
pm10_24 =(sum(data4(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 05-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 05-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 05-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 05-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 05-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 05-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data5 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-06.csv");
data5(25:27,:) = [];
data5(:,1) = [];
for i=1:24
    a = data5(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data5(:,1) = [];
data5(:,9) = data_kolumna;
data5 = table2array(data5);
data_kolumna = [];

roznica = data5 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data5(:,9)))/n;
pm10_24 =(sum(data5(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 06-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 06-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 06-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 06-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 06-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 06-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data6 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-07.csv");
data6(25:27,:) = [];
data6(:,1) = [];
for i=1:24
    a = data6(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data6(:,1) = [];
data6(:,9) = data_kolumna;
data6 = table2array(data6);
data_kolumna = [];

roznica = data6 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data6(:,9)))/n;
pm10_24 =(sum(data6(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 07-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 07-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 07-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 07-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 07-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 07-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data7 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-08.csv");
data7(25:27,:) = [];
data7(:,1) = [];
for i=1:24
    a = data7(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data7(:,1) = [];
data7(:,9) = data_kolumna;
data7 = table2array(data7);
data_kolumna = [];

roznica = data7 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data7(:,9)))/n;
pm10_24 =(sum(data7(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 08-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 08-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 08-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 08-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 08-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 08-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data8 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-09.csv");
data8(25:27,:) = [];
data8(:,1) = [];
for i=1:24
    a = data8(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data8(:,1) = [];
data8(:,9) = data_kolumna;
data8 = table2array(data8);
data_kolumna = [];

roznica = data8 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data8(:,9)))/n;
pm10_24 =(sum(data8(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 09-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 09-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 09-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 09-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 09-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 09-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data9 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-10.csv");
data9(25:27,:) = [];
data9(:,1) = [];
for i=1:24
    a = data9(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data9(:,1) = [];
data9(:,9) = data_kolumna;
data9 = table2array(data9);
data_kolumna = [];

roznica = data9 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data9(:,9)))/n;
pm10_24 =(sum(data9(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 10-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 10-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 10-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 10-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 10-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 10-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data10 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-11.csv");
data10(25:27,:) = [];
data10(:,1) = [];
for i=1:24
    a = data10(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data10(:,1) = [];
data10(:,9) = data_kolumna;
data10 = table2array(data10);
data_kolumna = [];

roznica = data10 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data10(:,9)))/n;
pm10_24 =(sum(data10(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 11-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 11-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 11-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 11-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 11-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 11-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data11 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-12.csv");
data11(25:27,:) = [];
data11(:,1) = [];
for i=1:24
    a = data11(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data11(:,1) = [];
data11(:,9) = data_kolumna;
data11 = table2array(data11);
data_kolumna = [];

roznica = data11 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data11(:,9)))/n;
pm10_24 =(sum(data11(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 12-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 12-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 12-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 12-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 12-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 12-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data12 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-13.csv");
data12(25:27,:) = [];
data12(:,1) = [];
for i=1:24
    a = data12(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data12(:,1) = [];
data12(:,9) = data_kolumna;
data12 = table2array(data12);
data_kolumna = [];

roznica = data12 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data12(:,9)))/n;
pm10_24 =(sum(data12(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 13-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 13-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 13-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 13-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 13-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 13-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data13 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-14.csv");
data13(25:27,:) = [];
data13(:,1) = [];
for i=1:24
    a = data13(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data13(:,1) = [];
data13(:,9) = data_kolumna;
data13 = table2array(data13);
data_kolumna = [];

roznica = data13 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data13(:,9)))/n;
pm10_24 =(sum(data13(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 14-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 14-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:24
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 14-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 14-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 14-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 14-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end


data14 = readtable("C:\Users\i5\Desktop\gitmatlab\air_quality_data_Zabrze\dane-pomiarowe_2020-11-15.csv");
data14(21:27,:) = [];
data14(:,1) = [];
for i=1:20
    a = data14(i,1);
    a = table2array(a);
    a = replace(a,',','.');
    a = cell2mat(a);
    a = string(a);
    a = double(a);
    data_kolumna(i,1) = a;
    
end
data_kolumna = table(data_kolumna);
data14(:,1) = [];
data14(:,9) = data_kolumna;
data14 = table2array(data14);
data_kolumna = [];

roznica = data14 - normy;
[n,m]=size(roznica);
no2 = roznica(:,1);
o3 = roznica(:,5);
co = roznica(:,7);
so2 = roznica(:,9);
so2_24 =(sum(data14(:,9)))/n;
pm10_24 =(sum(data14(:,8)))/n;

%wartość dobowa so2
if so2_24 > 125
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 15-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (so2_24 - 125));
        days = days + 1;
end
%wartość dobowa pm10
if pm10_24 > 50
    fprintf(result, "\nPrzekroczono dopuszczalną dobową średnią wartość stężenia PM10");
        fprintf(result, "\nDnia: 15-11-2020");
        fprintf(result, "\nO %d μg/m^3 \n", (pm10_24 - 50));
        days = days + 1;
end

for i = 1:20
    %wartość godzinna no2
    if no2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia NO2");
        fprintf(result, "\nDnia: 15-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", no2(i));
        hours = hours + 1;
    end
    %wartość godzinna so2
    if so2(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną jednogodzinną średnią wartość stężenia SO2");
        fprintf(result, "\nDnia: 15-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", so2(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna o3
    if o3(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia O3");
        fprintf(result, "\nDnia: 15-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", o3(i));
        hours = hours + 1;
    end
    %wartość ośmiogodzinna co
    if co(i) > 0
        fprintf(result, "\nPrzekroczono dopuszczalną ośmiogodzinną średnią wartość stężenia CO");
        fprintf(result, "\nDnia: 15-11-2020");
        fprintf(result, "\nO godzinie: %d:00", i);
        fprintf(result, "\nO %d μg/m^3 \n", co(i));
        hours = hours + 1;
    end
    
    
end

all_hours = days * 24 + hours;%zsumowanie liczby godzin w których dopuszczalna wartosć została przekroczona

%wypisanie liczby dni oraz godzin w których to nastąpiło
fprintf(result, "\nDni w których została przekroczona dopuszczona wartość średnia: %d", days);
fprintf(result, "\nIlość godzin w których została przekroczona dopiszczalna wartość średnia: %d", all_hours);

fclose(result);%zamknięcie pliku tekstowego



